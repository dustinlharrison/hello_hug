#!/bin/bash

ATTEMPT_LIMIT=999999999
cleanup() {
		kill %1 2>/dev/null
		i=$ATTEMPT_LIMIT
}

trap cleanup EXIT SIGINT

help() {
	echo "Usage: run_hello_hug command [max_attempts]"
	echo ""
	echo " COMMAND:"
	echo "   local - Serve the app on localhost:8000"
	echo "   cli   - Run the hug CLI for the app"
	echo "   test  - Run example tests against the app"
	echo ""
	echo " max_attempts limits the number of server restarts or tests"
}

log() {
	msg="${1}"
	echo ""
	echo "----------------------------------------------"
	echo " --> $msg"
	echo "----------------------------------------------"
	echo ""
}

APP="$( dirname __FILE__ )/hello_hug.py"
TEST="curl http://localhost:8000"

cmd=${1}
declare -i max_attempts=0
declare -i i=0

case $1 in 
    local)
	shift
	max_attempts=${1:-$(( $ATTEMPT_LIMIT - 1 ))}
	    while true; do
	    	hug -f $APP 
		i+=1
		if [ ${i} -ge ${max_attempts} ]; then
			break
		fi
		log "Relaunching after crash (${i})"
		sleep 2
	    done
    ;;
    cli)
	    shift
	    python $APP ${*}
    ;;
    test)
	shift
	max_attempts=${1:-0}

	while true; do
	    log "Demonstrate the return all bulletins handler"
	    $TEST/bulletins
	    log "Demonstrate a missing bulletin requested"
	    $TEST/bulletins/a
	    log "Demonstrate a valid bulletin returned"
	    $TEST/bulletins/b
	    log "Demonstrate missing or invalid endpoint/handler"
	    $TEST/

	    i+=1
	    if [ ${i} -ge ${max_attempts} ]; then
		exit
	    fi
	    log "Repeating tests  (${i})"
	    sleep 5
	done

    ;;
    *)
	    help
	    exit -1
    ;;
esac
