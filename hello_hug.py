
import hug

from marshmallow import Schema, fields
from marshmallow.validate import ValidationError

class Bulletin(object):
    '''
    A Bulletin object has:
        message - Display message for user in unicode
        b_id - ID of message
        order - Order of rotation for messages
    '''

    def __init__(self):
        self._message = u'Undefined'

    @property
    def message(self):
        '''
        message - Display message for user in unicode
        '''
        return self._message

    @message.setter
    def message(self, msg):
        self._message = msg

    @property
    def order(self):
        '''
        order - Order of rotation for messages
        '''
        return self._order

    @order.setter
    def order(self, order):
        self._order = order

    @property
    def b_id(self):
        '''
        b_id - ID of message
        '''
        return self._b_id

    @b_id.setter
    def b_id(self, b_id):
        self._b_id = b_id

class Bulletin(object):
    message = "Undefined"
    order = 0
    b_id = b'a'

class Bulletins(object):
    '''
    Container of Bulletin
    '''
    immutable_list = True

    def __init__(self):
        self._last_b_id = b'a'
        self._bulletins = {}

    def getNextID(self):
        self._last_b_id = bytes([self._last_b_id[0]+1])
        return self._last_b_id

    def query(self, b_id):
        if b_id:
            b_id = bytes(b_id,'ascii')
            return self._bulletins.get(b_id, [])

    @property
    def bulletins(self, b_id=None):
        return self._bulletins.values()

    @bulletins.setter
    def bulletins(self, msg):
        b_id=self.getNextID()
        bulletin=Bulletin()
        bulletin.order = len(self._bulletins) + 1
        bulletin.message = msg
        bulletin.b_id = b_id
        self._bulletins[b_id] = bulletin
        return b_id

# Custom validator
def must_not_be_blank(data):
    if not data:
        raise ValidationError('Field can not be blank.')


class BulletinSchema(Schema):
    b_id = fields.Str()
    order = fields.Int()
    message = fields.Str(validate=must_not_be_blank)

class BulletinsSchema(Schema):
    immutable_list = fields.Boolean()
    bulletins = fields.Nested(BulletinSchema, many=True)



sample=Bulletins()
sample.bulletins = "Warning!  Pigs caught flying!"
sample.bulletins = "Have you tried Kritchles yet?"
sample.bulletins = ""

bulletins_schema = BulletinsSchema(strict=True)
bulletin_schema = BulletinSchema(strict=True)

api_v2 = hug.get(versions=[2])

# Doesn't quite work...  Help module just defines a single handler for v2
@api_v2.urls('/bulletins')
@hug.get('/bulletins')
@hug.cli()
def get_bulletins(b_id=None):
    '''
    Returns all bulletins
    '''
    if b_id:
        return get_bulletin(b_id)

    result = bulletins_schema.dump(sample)
    try:
        bulletins_schema.validate(result.data)
    except (ValidationError) as e:
        result.errors.update(e.messages)

    return result

@hug.get('/bulletins/{b_id}')
@hug.cli()
def get_bulletin(b_id):
    '''
    Interact with a single bulletin
    '''
    bulletin = sample.query(b_id)
    result = bulletin_schema.dump(bulletin)
    try:
        bulletin_schema.validate(result.data)
    except (ValidationError) as e:
        result.errors.update(e.messages)

    return result

if __name__ == "__main__":

    import sys
    from pprint import pprint
    result = get_bulletins.interface.cli()
