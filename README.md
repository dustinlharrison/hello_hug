
Hello Hug!
===========

## Installation

You may need to install python3.6 (**f-strings!**).

```
git clone git@gitlab.com:dustinlharrison/hello_hug.git
cd hello_hug
mkvirtualenv -p python3.6 hello_hug
pip3 install -r requirements.pip
```

## Usage

```
╰─ ./run_hello_hug
Usage: run_hello_hug command

 COMMAND:
   local - Serve the app on localhost:8000
   cli   - Run the hug CLI for the app
   test  - Run example tests against the app
```

local
---
This is useful for live coding and developing.  It will re-launch the hug server if it crashes so that as you edit the source code and save you can see syntax errors or other messages.

test
---
This will constantly re-run the sample tests so you can see the results change while coding.

cli
---
This is the cli invocation of hug.  Change the code in ```__main__``` to adjust the behaviour.

## Examples


```
─ ./run_hello_hug test

----------------------------------------------
 --> Demonstrate the return all bulletins handler
----------------------------------------------

127.0.0.1 - - [23/Jun/2018 19:23:40] "GET /bulletins HTTP/1.1" 200 307
{"data": {"bulletins": [{"order": 1, "b_id": "b", "message": "Warning!  Pigs caught flying!"}, {"order": 2, "b_id": "c", "message": "Have you tried Kritchles yet?"}, {"order": 3, "b_id": "d", "message": ""}], "immutable_list": true}, "errors": {"bulletins": {"2": {"message": ["Field can not be blank."]}}}}
----------------------------------------------
 --> Demonstrate a missing bulletin requested
----------------------------------------------

127.0.0.1 - - [23/Jun/2018 19:23:40] "GET /bulletins/a HTTP/1.1" 200 26
{"data": {}, "errors": {}}
----------------------------------------------
 --> Demonstrate a valid bulletin returned
----------------------------------------------

127.0.0.1 - - [23/Jun/2018 19:23:40] "GET /bulletins/b HTTP/1.1" 200 93
{"data": {"order": 1, "b_id": "b", "message": "Warning!  Pigs caught flying!"}, "errors": {}}
----------------------------------------------
 --> Demonstrate missing or invalid endpoint/handler
----------------------------------------------

127.0.0.1 - - [23/Jun/2018 19:23:40] "GET / HTTP/1.1" 404 1469
{
    "404": "The API call you tried to make was not defined. Here's a definition of the API to help you get going :)",
    "documentation": {
        "version": 2,
        "versions": [
            2
        ],
        "handlers": {
            "/bulletins": {
                "GET": {
                    "usage": "\n    Returns all bulletins\n    ",
                    "examples": [
                        "http://localhost:8000/v2/bulletins"
                    ],
                    "outputs": {
                        "format": "JSON (Javascript Serialized Object Notation)",
                        "content_type": "application/json; charset=utf-8"
                    },
                    "inputs": {
                        "b_id": {
                            "type": "Basic text / string value"
                        }
                    }
                }
            },
            "/bulletins/{b_id}": {
                "GET": {
                    "usage": "\n    Interact with a single bulletin\n    ",
                    "outputs": {
                        "format": "JSON (Javascript Serialized Object Notation)",
                        "content_type": "application/json; charset=utf-8"
                    },
                    "inputs": {
                        "b_id": {
                            "type": "Basic text / string value"
                        }
                    }
                }
            }
        }
    }
}%
```
